package fi.vamk.tka;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Application prints Hello Monday world! to console
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println(helloWeekday("15/03/2020"));
    }

    /*
     * Method return the day of week name based on the given day string Method is
     * unfinished! You need to fix the errors!
     */
    public static String helloWeekday(String day) {
        try {
            // converting the given day string to a Date object
            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(day);
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            return (dayOfWeek + " " + weekday(dayOfWeek));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public static String weekday(int x) {
        String answer = null;
        switch (x) {
            case 1:
                answer = "Monday";
                break;
            case 2:
                answer = "Tuesday";
                break;
            case 3:
                answer = "Wednesday";
                break;
            case 4:
                answer = "Thursday";
                break;
            case 5:
                answer = "Friday";
                break;
            case 6:
                answer = "Saturday";
                break;
            case 7:
                answer = "Sunday";
                break;
        }
        return answer;
    }
}
